{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "tm.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "tm.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "tm.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "imagePullSecret" }}
{{- printf "{\"auths\": {\"%s\": {\"auth\": \"%s\"}}}" .Values.imageCredentials.registry (printf "%s:%s" .Values.imageCredentials.username .Values.imageCredentials.password | b64enc) | b64enc }}
{{- end }}


{{- define "env.connection.postgres" -}}
- name: POSTGRES_HOST
  value: {{ .Release.Name }}-postgresql
- name: POSTGRES_PORT_5432_TCP_ADDR
  value: $(POSTGRES_HOST)
- name: POSTGRES_ENV_POSTGRES_USER
  value: {{ .Values.postgresql.postgresUser }}
- name: POSTGRES_ENV_POSTGRES_USER
  value: {{ .Values.postgresql.postgresDatabase }}
- name: POSTGRES_ENV_POSTGRES_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-postgresql
      key: postgres-password
{{- end -}}

{{- define "env.connection.redis" -}}
- name: REDIS_HOST
  value: {{ .Release.Name }}-redis
- name: CHANNELS_REDIS_HOST
  value: $(REDIS_HOST)
- name: REDIS_CACHE_LOCATION
  value: redis://{{ .Release.Name }}-redis:6379/1
{{- end -}}

{{- define "env.connection.rabbitmq" -}}
- name: RABBITMQ_HOST
  value: {{ .Release.Name }}-rabbitmq
- name: RABBITMQ_USER
  value: user
- name: RABBITMQ_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-rabbitmq
      key: rabbitmq-password
{{- end -}}

{{- define "env.connection.celery" -}}
- name: CELERY_BROKER_URL
  value: amqp://$(RABBITMQ_USER):$(RABBITMQ_PASSWORD)@$(RABBITMQ_HOST):5672//
- name: CELERY_RESULT_BACKEND
  value: redis://$(REDIS_HOST):6379/0
{{- end -}}

{{- define "env.api" -}}
{{ include "env.connection.postgres" .}}
{{ include "env.connection.redis" .}}
{{ include "env.connection.rabbitmq" .}}
{{ include "env.connection.celery" .}}
- name: TEST
  value: TEST
{{- end -}}

{{- define "env.web" -}}
- name: API_SERVER_URL
  value: http://{{ .Release.Name }}-api
{{- end -}}