apiVersion: apps/v1beta2
kind: Deployment
metadata:
  name: {{ template "tm.fullname" . }}-api
  labels:
    app: {{ template "tm.name" . }}-api
    chart: {{ template "tm.chart" . }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app: {{ template "tm.name" . }}-api
      release: {{ .Release.Name }}
  template:
    metadata:
      labels:
        app: {{ template "tm.name" . }}-api
        release: {{ .Release.Name }}
    spec:
      imagePullSecrets:
        - name: {{ template "tm.fullname" . }}-dockerloginkey
      initContainers: 
        - name: migrate
          image: "{{ .Values.image.repository.api }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command:
            - "python"
          args:
            - "src/manage.py"
            - "migrate"
            - "--noinput"
          envFrom:    
            - configMapRef:
                name: {{ .Release.Name }}-api  
          env:
{{ include "env.api" . | indent 12}}
        - name: load-fixtures
          image: "{{ .Values.image.repository.api }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          workingDir: /app
          command:
            - "sh"
          args:
            - "/app/run/load_fixtures.sh"
          envFrom:    
            - configMapRef:
                name: {{ .Release.Name }}-api  
          env:
{{ include "env.api" . | indent 12}}
        - name: collectstatic
          image: "{{ .Values.image.repository.api }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command:
            - "python"
          args:
            - "/app/src/manage.py"
            - "collectstatic"
            - "--noinput"
          envFrom:    
            - configMapRef:
                name: {{ .Release.Name }}-api  
          env:
{{ include "env.api" . | indent 12}}
          volumeMounts:
            - mountPath: /tmp/tmapi_static
              name: static-data
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository.api }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          workingDir: /app/src
          command:
            {{- range .Values.api.command }}
              - {{ . }}
            {{- end }}
          args:
            {{- range .Values.api.args }}
              - {{ . }}
            {{- end }}
          ports:
            - name: http
              containerPort: 8000
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /api/common/topics/
              port: http
            initialDelaySeconds: 15
            timeoutSeconds: 10
            periodSeconds: 15
          readinessProbe:
            httpGet:
              path: /api/common/topics/
              port: http
          envFrom:    
            - configMapRef:
                name: {{ .Release.Name }}-api  
          env:
{{ include "env.api" . | indent 12}}
          volumeMounts:
            - mountPath: /app/media
              name: media-data
          resources:
{{ toYaml .Values.resources | indent 12 }}
      volumes:
        - name: static-data
          flexVolume:
            driver: rook.io/rook
            fsType: ceph
            options:
              fsName: {{ template "tm.fullname" . }}fs
              clusterNamespace: rook
              path: /{{ template "tm.fullname" . }}/static
        - name: media-data
          flexVolume:
            driver: rook.io/rook
            fsType: ceph
            options:
              fsName: {{ template "tm.fullname" . }}fs
              clusterNamespace: rook
              path: /{{ template "tm.fullname" . }}/media
    {{- with .Values.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.affinity }}
      affinity:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
